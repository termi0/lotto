const maxNumberToCreate = 42;
const maxnumberToPick = 6;

let playerPickedNumbers = [];
let generatedNumbers = [];
let numberPlayerGuess;
let playedGames = 0;
let UImostHitNumbers = 0;

window.onload = function() {

    var numberPoolHandler = document.querySelector('.poolOfNumbers');

    addNumberToThePool(numberPoolHandler, maxNumberToCreate);
    pickNumbers(numberPoolHandler);
    setingStartingUserInterface();
};
// Creating number pool
function addNumberToThePool(numberPoolHandler, maxNumberToCreate) {
    let numbers = '';

    for (let i = 1; i <= maxNumberToCreate; i++) {
        numbers += `<div class="number">${i}</div>`;
    }

    numberPoolHandler.innerHTML += numbers;
}

// Add clickable event on number
function pickNumbers(numberPoolHandler) {

    numberPoolHandler.addEventListener('click', function(e) {
        if (e.target && e.target.matches(".number")) {

            if (playerPickedNumbers.length < maxnumberToPick && playerPickedNumbers.includes(parseInt(e.target.innerHTML, 10)) != true) {

                playerPickedNumbers.push(parseInt(e.target.innerHTML, 10));
                e.target.classList.add('active');
                nunmberOfPickedNumbers(playerPickedNumbers);

            } else if (playerPickedNumbers.length == maxnumberToPick) {
                alert(`możesz wybrać tylko  ${maxnumberToPick}  liczb`);
            } else if (playerPickedNumbers.includes(parseInt(e.target.innerHTML, 10)) == true) {

                let indexOfElementToRemove = playerPickedNumbers.indexOf(parseInt(e.target.innerHTML, 10));

                e.target.classList.remove('active');
                playerPickedNumbers.splice(indexOfElementToRemove, 1);
                nunmberOfPickedNumbers(playerPickedNumbers);
            }
        }
    });
}

// Generate number that player must guess
function genereteWinnerNumbers(luckyStrike) {

    generatedNumbers = [];

    if (playerPickedNumbers.length == maxnumberToPick || luckyStrike == true) {

        while (generatedNumbers.length != maxnumberToPick) {

            let randomNumber = Math.round(Math.random() * 42)

            if (randomNumber > 0 && generatedNumbers.includes(randomNumber) != true) {
                generatedNumbers.push(randomNumber);
            }

        }

        return generatedNumbers;
    } else {
        alert(`musisz wybrać przynajmniej ${maxnumberToPick} liczb`)
    }


}

// Checking how many number player guess
function checkIfYouWin(playerPickedNumbers, generatedNumbers, numberPoolHandler) {

    numberPlayerGuess = 0;

    for (i = 0; i < playerPickedNumbers.length - 1; i++)
        if (generatedNumbers.includes(playerPickedNumbers[i])) {
            numberPlayerGuess++;
        }

    return numberPlayerGuess;
}

// Checking how many number player picked
function nunmberOfPickedNumbers(playerPickedNumbers) {

    document.querySelector('#nunmberOfPickedNumbers').innerHTML = `Wybrałeś liczb: ${playerPickedNumbers.length}`;
}

// Starting game
function startGame() {
    let activeNumberPoolHandler = document.querySelectorAll('.active');
    genereteWinnerNumbers(false);
    checkIfYouWin(playerPickedNumbers, generatedNumbers);
    mostHitNumbers(numberPlayerGuess);
    if (playerPickedNumbers.length == maxnumberToPick) {

        infoAfterGame(playerPickedNumbers, generatedNumbers, numberPlayerGuess, false, null, null);
        resetUIAfterGame(activeNumberPoolHandler);

    }
}

// Lucky strike generate random number for player and start game
function luckyStrike() {

    let activeNumberPoolHandler = document.querySelectorAll('.active');
    let luckyStrikePlayerNumbers = genereteWinnerNumbers(true);
    let luckyStrikeWinerNumbers = genereteWinnerNumbers(true);
    checkIfYouWin(luckyStrikePlayerNumbers, luckyStrikeWinerNumbers);
    mostHitNumbers(numberPlayerGuess);

    infoAfterGame(playerPickedNumbers, generatedNumbers, numberPlayerGuess, true, luckyStrikePlayerNumbers, luckyStrikeWinerNumbers);
    resetUIAfterGame(activeNumberPoolHandler);

}
// Set for user starting interface
function setingStartingUserInterface() {
    nunmberOfPickedNumbers(playerPickedNumbers);
    document.querySelector('#playedGames').innerHTML = `Rozegrałeś gier: ${playedGames}`;
    document.querySelector('#mostHitNumbers').innerHTML = `Najwięcej trafionych kul:  ${UImostHitNumbers}`;
}

// Set user interface
function infoAfterGame(playerPickedNumbers, generatedNumbers, numberPlayerGuess, luckyStrike, luckyStrikePlayerNumbers, luckyStrikeWinerNumbers) {
    if (luckyStrike == true) {
        document.querySelector('#playerNumbers').innerHTML = `Wybrane liczby: ${luckyStrikePlayerNumbers}`;
        document.querySelector('#generatedNumber').innerHTML = `Wylosowane liczby: ${luckyStrikeWinerNumbers}`;
        document.querySelector('#winnerInfo').innerHTML = `udało ci się trafić: ${numberPlayerGuess} liczb`;

    } else {
        document.querySelector('#playerNumbers').innerHTML = `Wybrane liczby: ${playerPickedNumbers}`;
        document.querySelector('#generatedNumber').innerHTML = `Wylosowane liczby: ${generatedNumbers}`;
        document.querySelector('#winnerInfo').innerHTML = `udało ci się trafić: ${numberPlayerGuess} liczb}`;
    }
    document.querySelector('#playedGames').innerHTML = `Rozegrałeś gier: ${++playedGames}`;
    document.querySelector('#mostHitNumbers').innerHTML = `Najwięcej trafionych kul: ${UImostHitNumbers}`;

}

// Reset player picked numbers
function resetUIAfterGame(activeNumberPoolHandler) {

    playerPickedNumbers = [];

    for (let i = 0; i < activeNumberPoolHandler.length; i++) {
        activeNumberPoolHandler[i].classList.remove('active');
    }

    nunmberOfPickedNumbers(playerPickedNumbers);

}

// Check greatest guess number
function mostHitNumbers(numberPlayerGuess) {

    if (numberPlayerGuess > UImostHitNumbers) {
        UImostHitNumbers = numberPlayerGuess;
    }

}